<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Gate;

class AdminController extends Controller
{

    public function add()
    {
    	if(!Gate::allows('isAdmin')){
            abort(404, "Sorry, you can't access this page!");
        };

    	return view('users.create');
    }

    public function create(Request $request, User $user)
	{
		$this->validate($request, [ 
            'name' => 'required', 
            'email' =>'required|email|unique:users,email,'. $user->id, 
            'user_type' =>'sometimes',
            'password' => 'required|min:6|confirmed' 
        ]);

   		User::create([
     	'name' => $request->name,
     	'email' => $request->email,
     	'user_type' => $request->user_type,
     	'password' => bcrypt($request->password),
   		]);

   		return view('welcome');
	}
 
}
