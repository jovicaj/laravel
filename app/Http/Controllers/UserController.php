<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request; 
use App\User;
use Gate;
use DB;

class UserController extends Controller
{
    //Edit user methods
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {   
        $user= User::find($user->id);

        return view('users.edit', compact('user'));
    }

    public function update(User $user, Request $request)
    {    
        $this->validate($request, [ 
            'name' => 'required|unique:users,name,'.$user->id, 
            'email' =>'required|email|unique:users,email,'. $user->id, 
            'user_type' =>'sometimes',
            'password' => 'required|min:6|confirmed' 
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->user_type = $request->user_type;
        $user->password = bcrypt($request->password);

        $user->save();

        return back();
    }
    //Delete user methods
    public function delete_form($user_id)
    {   
        return view('users.delete')->with('user_id', $user_id);
    }

    public function delete(Request $request)
    {   
        $user = User::where('id', $request->user_id)->first();

        if(auth()->user()->id == $request->user_id) {
            $user->delete();
            Auth::logout();
        } else {
            $user->delete();
        }

        return view('welcome')->with('global', 'Your account has been deleted!');
        
    } 
    //Dispay user methods
    public function display() 
    {
        if(!Gate::allows('isAdmin')){
            abort(404, "Sorry, you can't access this page!");
        };

        $users = User::all();

        return view('users.display')->with('users', $users); 
    }
}
