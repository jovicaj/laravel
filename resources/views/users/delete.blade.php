@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card bg-secondary">
                <div class="card-header"><h1 class="text-danger text-center">Delete account</h1></div>

                    <div class="card-body">
                	   <form method="post" action="{{route('users.delete')}}">
                		  {{ csrf_field() }} 
                        <input type="hidden" value="{{ $user_id}}" name="user_id">
                	    <h3 class="text-center">Are you sure you want to delete this account?</h3>
                	    <div class="form-group">
                	    <button type="submit" class="btn btn-block btn-lg btn-danger">Yes, delete this account</button>
                	    </div>
                	    </form>	
                	</div>
                </div>	
            </div>
        </div>
    </div>
</div>
@endsection                	
