@extends('layouts.app')

@section('content')


<div class="container">

	<table id="myTable" class="table table-striped display">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Type</th>
				<th>Date created</th>
				<th>Options</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>{{ $user->name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->user_type }}</td>
				<td>{{ $user->created_at->format('d-m-Y')}}</td>
				<td><button><a href="{{route('users.edit', $user->id)}}">Edit</a></button><button><a href="{{route('users.remove', $user->id)}}">Delete</a></button></td>		
			</tr>
			@endforeach	

		</tbody>
	</table>
</div>
</br></br>

@endsection

@section('scripts')
<script>	
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@endsection