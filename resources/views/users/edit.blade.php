@extends('layouts.app')

@section('content')

	<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card bg-secondary">
                <div class="card-header">Edit user</div>

                <div class="card-body">
                    <form method="post" action="{{route('users.update', $user->id)}}">
						{{ csrf_field() }}
   						{{ method_field('patch') }}

                        <div class="form-group ">
                        	<label for="name">Name</label>
                        	<input type="text" class="form-control" name="name"  value="{{ $user->name }}" />
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control"  value="{{ $user->email }}" />
                        </div>
            
                        <div class="form-group">
                            <label for="user_type">Role</label>
                            @can('isAdmin')
                            <input type="user_type" name="user_type" class="form-control" value="{{ $user->user_type}}"/>
                            @endcan
                            @if(!Gate::allows('isAdmin'))
                            <input readonly type="user_type" name="user_type" class="form-control" value="{{ $user->user_type}}"/>
                            <small>Only admin can change this field!</small>
                            @endif
                        </div>
                          
                        <div class="form-group">
                            <label for="password">Password</label>
                        	<input type="password" name="password" class="form-control"/>	
                        </div>
                        <div class="form-group">
                            <label for="password">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control"/>
                        </div>
                        <div class="form-group">    
                        	<button type="submit" class="btn-block btn-lg btn-success">Send</button>
                        </div>
                             
                    </form>
                </div>
            </div>
        </div>
    </div>                            	 	
@endsection



                            