<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Update user routes
Route::get('users/{user}', 'UserController@edit')->name('users.edit');

Route::patch('users/{user}/update', 'UserController@update')->name('users.update');

//Delete user routes
Route::get('user/{user_id}', 'UserController@delete_form')->name('users.remove');

Route::post('users/delete', 'UserController@delete')->name('users.delete');		

//Display users route
Route::get('users', 'UserController@display')->name('users.display');

//Adding a new user as admin
Route::get('new/user', 'AdminController@add')->name('new.user'); 

Route::post('create', 'AdminController@create')->name('create.user');